'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
class DomainEventDispatcher {
  constructor() {
    this.callbacks = {};
    this.callbacksOnce = {};
  }

  // noinspection JSUnusedGlobalSymbols
  subscribe(type, listenerCallback) {
    DomainEventDispatcher._addListenerCallback(this.callbacks, type, listenerCallback);
  }

  subscribeOnce(type, listenerCallback) {
    DomainEventDispatcher._addListenerCallback(this.callbacksOnce, type, listenerCallback);
  }

  // noinspection JSUnusedGlobalSymbols
  unsubscribe(type, listenerCallback) {
    DomainEventDispatcher._removeListenerCallback(this.callbacks, type, listenerCallback);
    DomainEventDispatcher._removeListenerCallback(this.callbacksOnce, type, listenerCallback);
  }

  // noinspection JSUnusedGlobalSymbols
  dispatchEvent(event) {
    if (this.callbacks.hasOwnProperty(event.type)) {
      this.callbacks[event.type].forEach(listenerCallback => listenerCallback(event));
    }
    if (this.callbacks.hasOwnProperty('*')) {
      this.callbacks['*'].forEach(listenerCallback => listenerCallback(event));
    }
    if (this.callbacksOnce.hasOwnProperty(event.type)) {
      this.callbacksOnce[event.type].forEach(listenerCallback => listenerCallback(event));
      delete this.callbacksOnce[event.type];
    }
  }

  static _addListenerCallback(callbackContainer, type, listenerCallback) {
    if (!callbackContainer.hasOwnProperty(type)) {
      callbackContainer[type] = [];
    }
    callbackContainer[type].push(listenerCallback);
  }

  static _removeListenerCallback(callbackContainer, type, listenerCallback) {
    if (callbackContainer.hasOwnProperty(type)) {
      callbackContainer[type] = callbackContainer[type].filter(cb => cb !== listenerCallback);
    }
  }
}
exports.default = DomainEventDispatcher;