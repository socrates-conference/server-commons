'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _eventDispatcher = require('./eventDispatcher');

class Aggregate {

  constructor(dispatcher) {
    this._dispatcher = dispatcher;
  }

  // noinspection JSUnusedGlobalSymbols
  dispatch(event) {
    this._dispatcher.dispatchEvent(event);
  }
}
exports.default = Aggregate;