'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _app = require('./app');

var _app2 = _interopRequireDefault(_app);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _rootServer = require('./routes/rootServer');

var _rootServer2 = _interopRequireDefault(_rootServer);

var _defaultHeaders = require('./routes/defaultHeaders');

var _defaultHeaders2 = _interopRequireDefault(_defaultHeaders);

var _bodyParsers = require('./routes/bodyParsers');

var _bodyParsers2 = _interopRequireDefault(_bodyParsers);

var _authenticationMiddleware = require('./routes/authenticationMiddleware');

var _authenticationMiddleware2 = _interopRequireDefault(_authenticationMiddleware);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

describe('Application:', () => {
  let app;
  const defaultConfiguration = {
    environment: 'test',
    jwtSecret: _config2.default.jwtSecret,
    auth: {
      needsAuthentication: ['/authrequired'],
      needsAdministrator: ['/adminrequired']
    },
    server: {
      port: 7777
    }
  };

  beforeAll(_asyncToGenerator(function* () {
    app = new _app2.default(defaultConfiguration);
    app.applyMiddleware(_bodyParsers2.default);
    app.applyMiddleware(_defaultHeaders2.default);
    app.applyMiddleware((0, _authenticationMiddleware2.default)());
    app.applyMiddleware(mockPaths);
    app.applyMiddleware((0, _rootServer2.default)());
    yield app.run();
  }));

  afterAll(_asyncToGenerator(function* () {
    if (app) {
      yield app.stop();
    }
  }));

  it('starts', () => {
    expect(app.isRunning).toBe(true);
  });

  it('accepts get requests to root', () => {
    return _axios2.default.get('http://localhost:7777/').then(response => {
      expect(response.status).toBe(200);
    });
  });

  it('rejects requests to non existent urls', done => {
    _axios2.default.get('http://localhost:7777/non-existent')
    // $FlowFixMe somehow does not resolve 'fail'
    .then(res => fail('Should have returned an error code, instead responded', res)).catch(err => {
      expect(err.response.status).toBe(404);
      done();
    });
  });

  it('allows authorized requests to auth restricted urls', () => {
    const requestToken = _jsonwebtoken2.default.sign({ name: 'user', email: 'name@domain.de', hash: 'password' }, _config2.default.jwtSecret);
    return _axios2.default.get('http://localhost:7777/authRequired', { headers: { authorization: 'Bearer ' + requestToken } }).then(response => {
      expect(response.status).toBe(200);
    });
  });

  it('rejects anonymous requests to auth restricted urls', done => {
    _axios2.default.get('http://localhost:7777/authRequired')
    // $FlowFixMe somehow does not resolve 'fail'
    .then(res => fail('Should have returned an error code, instead responded', res)).catch(err => {
      expect(err.response.status).toBe(401);
      done();
    });
  });

  it('allows authorized requests to admin restricted urls', () => {
    const requestToken = _jsonwebtoken2.default.sign({ name: 'admin', email: 'name@domain.de', hash: 'password', isAdministrator: true }, _config2.default.jwtSecret);
    return _axios2.default.get('http://localhost:7777/adminRequired', { headers: { authorization: 'Bearer ' + requestToken } }).then(response => {
      expect(response.status).toBe(200);
    });
  });

  it('rejects anonymous requests to admin restricted urls', done => {
    _axios2.default.get('http://localhost:7777/adminRequired')
    // $FlowFixMe somehow does not resolve 'fail'
    .then(res => fail('Should have returned an error code, instead responded', res)).catch(err => {
      expect(err.response.status).toBe(401);
      done();
    });
  });

  it('rejects unauthorized requests to admin restricted urls', done => {
    const requestToken = _jsonwebtoken2.default.sign({ name: 'user', email: 'name@domain.de', hash: 'password' }, _config2.default.jwtSecret);
    _axios2.default.get('http://localhost:7777/adminRequired', { headers: { authorization: 'Bearer ' + requestToken } })
    // $FlowFixMe somehow does not resolve 'fail'
    .then(res => fail('Should have returned an error code, instead responded', res)).catch(err => {
      expect(err.response.status).toBe(401);
      done();
    });
  });
});

function mockPaths(app) {
  app.get('/authRequired', (req, res) => {
    res.status(200).end();
  });

  app.get('/adminRequired', (req, res) => {
    res.status(200).end();
  });
}