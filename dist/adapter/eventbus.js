'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _socratesEventStream = require('socrates-event-stream');

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _consoleLogger = require('../consoleLogger');

var _consoleLogger2 = _interopRequireDefault(_consoleLogger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class EventBus {

  constructor(eventDispatcher, kafka, logger = _consoleLogger2.default) {
    this._handleReady = () => {
      this._logger.info('Connected to Kafka on %s:%s', _config2.default.kafka.host, _config2.default.kafka.port);
      this._stringify.pipe(this._kafka);
      this._eventDispatcher.subscribe('*', this._handleEvent);
    };

    this._handleEvent = ev => {
      const timestamp = new Date();
      const publishedEvent = {
        event: ev.type,
        value: ev.payload,
        uuid: (0, _v2.default)(timestamp),
        timestamp
      };
      this._stringify.write(publishedEvent);
    };

    this._handleError = err => {
      this._logger.error('Error while streaming to Kafka: %o', err);
    };

    this._eventDispatcher = eventDispatcher;
    this._kafka = kafka;
    this._kafka.on('error', this._handleError);
    this._kafka.on('ready', this._handleReady);
    this._stringify = new _socratesEventStream.StringifyTransform();
    this._logger = logger;
  }

}
exports.default = EventBus;