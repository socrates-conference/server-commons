'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _rc = require('rc');

var _rc2 = _interopRequireDefault(_rc);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// add your own config in .socrates-serverrc file and don't add it to git if you want to keep your secrets.

const config = {
  environment: 'dev',
  jwtSecret: '$lsRTf!gksTRcDWs',
  database: {
    host: '',
    port: 0,
    name: '',
    user: '',
    password: ',',
    debug: false
  },
  server: {
    port: 4444
  },
  kafka: {
    host: 'zookeeper',
    port: 2181,
    topics: []
  }
};

exports.default = (0, _rc2.default)('server-commons', config);