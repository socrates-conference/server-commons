'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _middleware = require('./middleware');

var _consoleLogger = require('./consoleLogger');

var _consoleLogger2 = _interopRequireDefault(_consoleLogger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class App {

  constructor(config, logger = _consoleLogger2.default) {
    this.applyMiddleware = middleware => {
      (0, _middleware.addMiddleware)(this.app, this.config)(middleware);
    };

    this.run = () => {
      return new Promise(resolve => {
        this.isRunning = true;
        this.server = this.app.listen(this.config.server.port, () => {
          this.logger.info(`Server listening on port ${this.config.server.port}`);
          resolve();
        });
      });
    };

    this.stop = () => {
      return new Promise(resolve => {
        this.isRunning = false;
        if (this.server) {
          this.server.close(() => {
            this.logger.info('Server shutdown complete.');
            resolve();
          });
        }
      });
    };

    this.isRunning = false;
    this.config = config;
    this.app = (0, _express2.default)();
    this.app.set('env', config.environment);
    this.logger = logger;
  }

}
exports.default = App;