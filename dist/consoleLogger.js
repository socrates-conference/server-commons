'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});


const consoleLogger = {
  error: (msg, ...args) => console.error(msg, args),
  info: (msg, ...args) => console.info(msg, args),
  debug: (msg, ...args) => console.log(msg, args),
  trace: (msg, ...args) => console.trace(msg, args)
};
exports.default = consoleLogger;