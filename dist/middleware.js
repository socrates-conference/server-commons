'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addMiddleware = addMiddleware;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function addMiddleware(app, config) {
  return fn => fn(app, config);
}