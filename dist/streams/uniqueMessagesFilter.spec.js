'use strict';

var _uniqueMessagesFilter = require('./uniqueMessagesFilter');

var _uniqueMessagesFilter2 = _interopRequireDefault(_uniqueMessagesFilter);

var _testLogger = require('../testLogger');

var _testLogger2 = _interopRequireDefault(_testLogger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let event;
let stream;

describe('UniqueMessagesFilter:', () => {

  beforeEach(() => {
    event = {
      uuid: '1',
      event: 'ALLOW_INVITATION',
      timestamp: new Date(),
      value: {}
    };
    stream = new _uniqueMessagesFilter2.default(_testLogger2.default);
  });

  it('should pass unique invitation message once', done => {
    stream.on('data', data => {
      expect(data).toEqual(event);
      done();
    });

    stream.write(event);
  });

  it('should not pass duplicate messages', done => {
    let i = 0;
    stream.on('data', () => {
      i++;
      console.log('data', i);
      if (i > 1) {
        // $FlowFixMe Flow somehow does not recognize fail(), but it is a valid method and part of jest.
        fail(new Error('should not pass the same message twice'));
      }
    });
    stream.write(event);
    stream.write(event);
    done();
  });
});