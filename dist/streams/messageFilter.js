'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stream = require('stream');

var _stream2 = _interopRequireDefault(_stream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class MessagesFilter extends _stream2.default.Transform {

  constructor(types, logger) {
    super({ objectMode: true });
    this._logger = logger;
    this._types = types;
    this._initEventHandlers();
  }

  _initEventHandlers() {
    this.on('error', err => {
      this._logger.error('Error while using MessageFilter stream', err);
    });

    this.on('close', () => {
      this._logger.info('MessageFilter stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('MessageFilter stream finished.');
    });
  }

  _transform(chunk, encoding, callback) {
    if (this._types.indexOf(chunk.event) !== -1) {
      this.push(chunk);
    }
    callback();
  }
}
exports.default = MessagesFilter;