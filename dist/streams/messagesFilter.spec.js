'use strict';

var _messageFilter = require('./messageFilter');

var _messageFilter2 = _interopRequireDefault(_messageFilter);

var _testLogger = require('../testLogger');

var _testLogger2 = _interopRequireDefault(_testLogger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let event;
let stream;
const types = ['ALLOW_INVITATION'];

describe('MessageFilter:', () => {

  beforeEach(() => {
    event = {
      uuid: '1',
      event: 'ALLOW_INVITATION',
      timestamp: new Date(),
      value: {}
    };
    stream = new _messageFilter2.default(types, _testLogger2.default);
  });

  it('should pass sepecified messages', done => {
    stream.on('data', data => {
      expect(data).toEqual(event);
      done();
    });

    stream.write(event);
  });

  it('should not pass other messages', done => {
    event['event'] = 'OTHER';
    stream.on('data', () => {
      // $FlowFixMe Flow somehow does not recognize fail(), but it is a valid method and part of jest.
      fail(new Error('should not pass non-specified messages'));
    });

    stream.write(event);
    done();
  });
});