'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stream = require('stream');

var _stream2 = _interopRequireDefault(_stream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class UniqueMessagesFilter extends _stream2.default.Transform {

  constructor(logger) {
    super({ objectMode: true });
    this._logger = logger;
    this._processedMessages = [];
    this._initEventHandlers();
  }

  _initEventHandlers() {
    this.on('error', err => {
      this._logger.error('Error while using UniqueMessagesFilter stream', err);
    });

    this.on('close', () => {
      this._logger.info('UniqueMessagesFilter stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('UniqueMessagesFilter stream finished.');
    });
  }

  _transform(chunk, encoding, callback) {
    this._processChunkIfUnique(chunk);
    callback();
  }

  _processChunkIfUnique(event) {
    if (!(this._processedMessages.indexOf(event.uuid) !== -1)) {
      this._processedMessages.push(event.uuid);
      this.push(event);
    }
  }
}
exports.default = UniqueMessagesFilter;