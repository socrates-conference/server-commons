'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});


const testLogger = {
  error: () => {},
  info: () => {},
  debug: () => {},
  trace: () => {}
};
exports.default = testLogger;