'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createAuthenticationMiddleware;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _jsonwebtoken = require('jsonwebtoken');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const isJWTAuthorizationAvailable = req => {
  return req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer';
};

const extractJWTToken = req => {
  return req.headers.authorization.split(' ')[1];
};

function createAuthenticationMiddleware() {
  return (app, config) => {
    app.use((() => {
      var _ref = _asyncToGenerator(function* (req, res, next) {

        if (req.method === 'OPTIONS') {
          return next();
        }
        const requestNeedsAuthentication = config && config.auth && config.auth.needsAuthentication || [];
        const requestNeedsAdministrator = config && config.auth && config.auth.needsAdministrator || [];

        const needsAuthentication = function (url) {
          const pattern = new RegExp(requestNeedsAuthentication.join('|'));
          return pattern.test(url.toLowerCase());
        };

        const needsAdministrator = function (url) {
          const pattern = new RegExp(requestNeedsAdministrator.join('|'));
          return pattern.test(url.toLowerCase());
        };

        const populateRequestUser = function () {
          if (isJWTAuthorizationAvailable(req)) {
            if (!config || !config.jwtSecret) {
              throw new Error('Configuration error: No JWT secret found.');
            }
            (0, _jsonwebtoken.verify)(extractJWTToken(req), config.jwtSecret, function (err, decode) {
              req.user = err ? undefined : decode;
            });
          } else {
            req.user = undefined;
          }
        };

        const checkAccess = (() => {
          var _ref2 = _asyncToGenerator(function* (url, theUser) {
            const authenticationRequired = needsAuthentication(url);
            const administratorRequired = needsAdministrator(url);

            if (!theUser && (authenticationRequired || administratorRequired)) {
              return { allowed: false, error: new Error('Authentication required.') };
            }
            if (theUser && administratorRequired && !theUser.isAdministrator) {
              return { allowed: false, error: new Error('Administrator required.') };
            }
            return { allowed: true, error: null };
          });

          return function checkAccess(_x4, _x5) {
            return _ref2.apply(this, arguments);
          };
        })();
        populateRequestUser();
        const access = yield checkAccess(req.url, req.user);
        if (access.allowed) {
          return next();
        } else {
          res.status(401).send(access.error);
        }
      });

      return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    })());
  };
}