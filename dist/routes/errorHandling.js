'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tryToProcess = exports.throwServerError = undefined;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _testLogger = require('../testLogger');

var _testLogger2 = _interopRequireDefault(_testLogger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const throwServerError = exports.throwServerError = (status, message) => {
  // This ugly workaround is brought to you by Babel's inability to extend the built-in Error class.
  throw new Error(JSON.stringify({ status, message }));
};

const tryToProcess = exports.tryToProcess = (req, res, action) => {
  return action(req.body).then(() => res.status(200).end()).catch(err => {
    // This ugly exception handler is brought to you by Babel's inability to extend the built-in Error class.
    try {
      const { status, message } = JSON.parse(err.message);
      return res.status(status).send(message).end();
    } catch (_) {
      _testLogger2.default.error('Error while processing request: %o', err);
      return res.status(500).end();
    }
  });
};