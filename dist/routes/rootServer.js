'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createRootServer;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createRootServer() {
  return app => {
    app.get('/', (req, res) => {
      res.status(200).json({ 'status': 'Server UP' }).end();
    });
    app.use(pageNotFound);
  };
}

// catch 404 and forward to error handler

function pageNotFound(req, res) {
  const err = new Error('Not Found');
  // noinspection JSUndefinedPropertyAssignment
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') !== 'prod' ? err : {};
  // render the error page
  res.status(404);
  res.send(req.app.get('env') !== 'prod' ? err : '');
}