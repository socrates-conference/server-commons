'use strict';

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('config', () => {
  it('contains server object', () => {
    expect(_config2.default.server).toBeDefined();
  });
  it('defines server port', () => {
    expect(_config2.default.server.port).toBeDefined();
  });
  it('contains environment definition', () => {
    expect(_config2.default.environment).toBeDefined();
  });
  it('contains jwt secret definition', () => {
    expect(_config2.default.environment).toBeDefined();
  });
  it('contains database object', () => {
    expect(_config2.default.jwtSecret).toBeDefined();
  });
  it('defines database host', () => {
    expect(_config2.default.database.host).toBeDefined();
  });
  it('defines database port', () => {
    expect(_config2.default.database.port).toBeDefined();
  });
  it('defines database name', () => {
    expect(_config2.default.database.name).toBeDefined();
  });
  it('defines database user', () => {
    expect(_config2.default.database.user).toBeDefined();
  });
  it('defines database password', () => {
    expect(_config2.default.database.password).toBeDefined();
  });
});