'use strict';

var _index = require('./index');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

describe('server-commons library:', () => {
  describe('should expose modules for import', () => {
    it('for use with flow types, and without errors or warnings', _asyncToGenerator(function* () {
      // ignore test logic; this test should produce flow errors if the type signatures don't match
      const event = { type: 'something', payload: {} };
      const listener = function (ev) {
        return expect(ev.payload).not.toBeNull();
      };
      listener(event);
      const dispatcher = new _index.DomainEventDispatcher();
      const aggregate = new _index.Aggregate(dispatcher);
      expect(aggregate).not.toBeNull();

      const typedConfig = _index.config;
      const app = new _index.App(typedConfig);
      app.applyMiddleware(_index.defaultHeaders);
      app.applyMiddleware(_index.bodyParsers);
      app.applyMiddleware((0, _index.createAuthenticationMiddleware)());
      app.applyMiddleware((0, _index.createRootServer)());

      if (app.config.kafka) {
        expect(app.config.kafka.host).not.toBeNull();
      }

      expect(new _index.EventBus(dispatcher, { on: function () {} })).not.toBeNull();

      yield new Promise(function (resolve) {
        const response = {
          status: function (status) {
            expect(status).toBe(200);
            return response;
          }, end: function () {
            return resolve('OK');
          }
        };
        const request = { body: { status: 'OK' } };
        (0, _index.tryToProcess)(request, response, function (status) {
          return Promise.resolve(status);
        });
      }).then(function (result) {
        return expect(result).toBe('OK');
      });

      try {
        (0, _index.throwServerError)(400, 'invalid');
      } catch (e) {
        expect(e.message).toEqual('{"status":400,"message":"invalid"}');
      }

      let logger = _index.consoleLogger;
      logger.info('info');
      logger = _index.testLogger;
      logger.info('info');

      const unique = new _index.UniqueMessagesFilter(logger);
      const filter = new _index.MessageFilter(['something'], logger);
      unique.pipe(filter);
      const messages = [];
      filter.on('data', function (chunk) {
        messages.push(chunk);
      });

      const chunk1 = { id: 1, event: 'something' };
      unique.write(chunk1);
      unique.write(chunk1);
      const chunk2 = { id: 2, event: 'somethingElse' };
      unique.write(chunk2);

      expect(messages).toHaveLength(1);
      expect(messages[0].id).toEqual(1);
    }));
  });
});