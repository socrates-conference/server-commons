//@flow

import {
  Aggregate,
  App,
  bodyParsers,
  config,
  createAuthenticationMiddleware,
  createRootServer,
  defaultHeaders,
  DomainEventDispatcher,
  Event,
  EventBus,
  EventDispatcher, tryToProcess, throwServerError, consoleLogger, testLogger, MessageFilter, UniqueMessagesFilter
} from './index';

import type {
  Config,
  Listener,
  Logger
} from './index';

describe('server-commons library:', () => {
  describe('should expose modules for import', () => {
    it('for use with flow types, and without errors or warnings', async () => {
      // ignore test logic; this test should produce flow errors if the type signatures don't match
      const event: Event = {type: 'something', payload: {}};
      const listener: Listener = (ev: Event) => expect(ev.payload).not.toBeNull();
      listener(event);
      const dispatcher: EventDispatcher = new DomainEventDispatcher();
      const aggregate = new Aggregate(dispatcher);
      expect(aggregate).not.toBeNull();

      const typedConfig: Config = config;
      const app: App = new App(typedConfig);
      app.applyMiddleware(defaultHeaders);
      app.applyMiddleware(bodyParsers);
      app.applyMiddleware(createAuthenticationMiddleware());
      app.applyMiddleware(createRootServer());

      if (app.config.kafka) {
        expect(app.config.kafka.host).not.toBeNull();
      }

      expect(new EventBus(dispatcher, {on: () => {}})).not.toBeNull();

      await new Promise(resolve => {
        const response = {
          status: status => {
            expect(status).toBe(200);
            return response;
          }, end: () => resolve('OK')
        };
        const request = {body: {status: 'OK'}};
        tryToProcess(request, response, status => Promise.resolve(status));
      }).then(result => expect(result).toBe('OK'));

      try {
        throwServerError(400, 'invalid');
      } catch (e) {
        expect(e.message).toEqual('{"status":400,"message":"invalid"}');
      }

      let logger: Logger = consoleLogger;
      logger.info('info');
      logger = testLogger;
      logger.info('info');

      const unique = new UniqueMessagesFilter(logger);
      const filter = new MessageFilter(['something'], logger);
      unique.pipe(filter);
      const messages = [];
      filter.on('data', chunk => {
        messages.push(chunk);
      });

      const chunk1: any = {id: 1, event: 'something'};
      unique.write(chunk1);
      unique.write(chunk1);
      const chunk2: any = {id: 2, event: 'somethingElse'};
      unique.write(chunk2);

      expect(messages).toHaveLength(1);
      expect(messages[0].id).toEqual(1);
    });
  });
});
