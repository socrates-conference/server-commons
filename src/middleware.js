// @flow

import express from 'express';
import type {Config} from './ConfigType';

export type Middleware = (app: express.Application, config?: Config) => void;

export function addMiddleware(app: express.Application, config?: Config) {
  return (fn: Middleware) => fn(app, config);
}