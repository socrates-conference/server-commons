// @flow
import type {Logger} from './logger';

const consoleLogger: Logger = {
  error: (msg, ...args) => console.error(msg, args),
  info: (msg, ...args) => console.info(msg, args),
  debug: (msg, ...args) => console.log(msg, args),
  trace: (msg, ...args) => console.trace(msg, args)
};

export default consoleLogger;