// @flow

import type {
  Event,
  EventDispatcher
} from '../domain/eventDispatcher';
import {
  EventPublisher,
  StringifyTransform
} from 'socrates-event-stream';
import config from '../config';
import uuid from 'uuid/v4';
import type {Logger} from '../logger';
import consoleLogger from '../consoleLogger';
import type {SoCraTesEvent} from '..';

export default class EventBus {
  _eventDispatcher: EventDispatcher;
  _kafka: EventPublisher;
  _stringify: StringifyTransform;
  _logger: Logger;

  constructor(eventDispatcher: EventDispatcher, kafka: EventPublisher, logger: Logger = consoleLogger) {
    this._eventDispatcher = eventDispatcher;
    this._kafka = kafka;
    this._kafka.on('error', this._handleError);
    this._kafka.on('ready', this._handleReady);
    this._stringify = new StringifyTransform();
    this._logger = logger;
  }

  _handleReady = () => {
    this._logger.info('Connected to Kafka on %s:%s', config.kafka.host, config.kafka.port);
    this._stringify.pipe(this._kafka);
    this._eventDispatcher.subscribe('*', this._handleEvent);
  };

  _handleEvent = (ev: Event) => {
    const timestamp = new Date();
    const publishedEvent: SoCraTesEvent = {
      event: ev.type,
      value: ev.payload,
      uuid: uuid(timestamp),
      timestamp
    };
    this._stringify.write(publishedEvent);
  };

  _handleError = (err: Error) => {
    this._logger.error('Error while streaming to Kafka: %o', err);
  };

}