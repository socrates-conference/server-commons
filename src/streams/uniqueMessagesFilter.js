// @flow

import stream from 'stream';
import type {SoCraTesEvent} from './socratesEvent';
import type {Logger} from '../logger';

export default class UniqueMessagesFilter extends stream.Transform {
  _logger: Logger;
  _processedMessages: string[];

  constructor(logger: Logger) {
    super({objectMode: true});
    this._logger = logger;
    this._processedMessages = [];
    this._initEventHandlers();
  }

  _initEventHandlers(): void {
    this.on('error', err => {
      this._logger.error('Error while using UniqueMessagesFilter stream', err);
    });

    this.on('close', () => {
      this._logger.info('UniqueMessagesFilter stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('UniqueMessagesFilter stream finished.');
    });
  }

  _transform(chunk: any, encoding: string, callback: (err?: Error, data?: any) => void): void {
    this._processChunkIfUnique(chunk);
    callback();
  }

  _processChunkIfUnique(event: SoCraTesEvent) {
    if (!this._processedMessages.includes(event.uuid)) {
      this._processedMessages.push(event.uuid);
      this.push((event: any));
    }
  }
}