// @flow

import UniqueMessagesFilter from './uniqueMessagesFilter';
import testLogger from '../testLogger';

let event: any;
let stream: UniqueMessagesFilter;

describe('UniqueMessagesFilter:', () => {

  beforeEach(() => {
    event = {
      uuid: '1',
      event: 'ALLOW_INVITATION',
      timestamp: new Date(),
      value: {}
    };
    stream = new UniqueMessagesFilter(testLogger);
  });

  it('should pass unique invitation message once', done => {
    stream.on('data', data => {
      expect(data).toEqual(event);
      done();
    });

    stream.write(event);
  });

  it('should not pass duplicate messages', done => {
    let i = 0;
    stream.on('data', () => {
      i++;
      console.log('data', i);
      if (i > 1) {
        // $FlowFixMe Flow somehow does not recognize fail(), but it is a valid method and part of jest.
        fail(new Error('should not pass the same message twice'));
      }
    });
    stream.write(event);
    stream.write(event);
    done();
  });
});