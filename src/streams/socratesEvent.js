// @flow

export type SoCraTesEvent = {
  uuid: string,
  event: string,
  value: any,
  timestamp: Date
}