// @flow

import stream from 'stream';
import type {SoCraTesEvent} from './socratesEvent';
import type {Logger} from '../logger';


export default class MessagesFilter extends stream.Transform {
  _logger: Logger;
  _types: string[];

  constructor(types: string[], logger: Logger) {
    super({objectMode: true});
    this._logger = logger;
    this._types = types;
    this._initEventHandlers();
  }

  _initEventHandlers(): void {
    this.on('error', err => {
      this._logger.error('Error while using MessageFilter stream', err);
    });

    this.on('close', () => {
      this._logger.info('MessageFilter stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('MessageFilter stream finished.');
    });
  }

  _transform(chunk: any, encoding: string, callback: (err?: Error, data?: any) => void): void {
    if (this._types.includes((chunk: SoCraTesEvent).event)) {
      this.push(chunk);
    }
    callback();
  }
}