// @flow

import MessageFilter from './messageFilter';
import testLogger from '../testLogger';

let event: any;
let stream: MessageFilter;
const types = ['ALLOW_INVITATION'];

describe('MessageFilter:', () => {

  beforeEach(() => {
    event = {
      uuid: '1',
      event: 'ALLOW_INVITATION',
      timestamp: new Date(),
      value: {}
    };
    stream = new MessageFilter(types, testLogger);
  });

  it('should pass sepecified messages', done => {
    stream.on('data', data => {
      expect(data).toEqual(event);
      done();
    });

    stream.write(event);
  });

  it('should not pass other messages', done => {
    event['event'] = 'OTHER';
    stream.on('data', () => {
      // $FlowFixMe Flow somehow does not recognize fail(), but it is a valid method and part of jest.
      fail(new Error('should not pass non-specified messages'));
    });

    stream.write(event);
    done();
  });
});