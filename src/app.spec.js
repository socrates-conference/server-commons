// @flow
import type {Config} from './ConfigType';
import express from 'express';
import App from './app';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import config from './config';
import createRootServer from './routes/rootServer';
import defaultHeaders from './routes/defaultHeaders';
import bodyParsers from './routes/bodyParsers';
import createAuthenticationMiddleware from './routes/authenticationMiddleware';

describe('Application:', () => {
  let app;
  const defaultConfiguration: Config = {
    environment: 'test',
    jwtSecret: config.jwtSecret,
    auth: {
      needsAuthentication: ['/authrequired'],
      needsAdministrator: ['/adminrequired']
    },
    server: {
      port: 7777
    }
  };

  beforeAll(async () => {
    app = new App(defaultConfiguration);
    app.applyMiddleware(bodyParsers);
    app.applyMiddleware(defaultHeaders);
    app.applyMiddleware(createAuthenticationMiddleware());
    app.applyMiddleware(mockPaths);
    app.applyMiddleware(createRootServer());
    await app.run();
  });

  afterAll(async () => {
    if (app) {
      await app.stop();
    }
  });

  it('starts', () => {
    expect(app.isRunning).toBe(true);
  });

  it('accepts get requests to root', () => {
    return axios.get('http://localhost:7777/')
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  it('rejects requests to non existent urls', (done) => {
    axios.get('http://localhost:7777/non-existent')
    // $FlowFixMe somehow does not resolve 'fail'
      .then(res => fail('Should have returned an error code, instead responded', res))
      .catch(err => {
        expect(err.response.status).toBe(404);
        done();
      });
  });

  it('allows authorized requests to auth restricted urls', () => {
    const requestToken = jwt.sign({name: 'user', email: 'name@domain.de', hash: 'password'}, config.jwtSecret);
    return axios.get('http://localhost:7777/authRequired', {headers: {authorization: 'Bearer ' + requestToken}})
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  it('rejects anonymous requests to auth restricted urls', done => {
    axios.get('http://localhost:7777/authRequired')
    // $FlowFixMe somehow does not resolve 'fail'
      .then(res => fail('Should have returned an error code, instead responded', res))
      .catch(err => {
        expect(err.response.status).toBe(401);
        done();
      });
  });

  it('allows authorized requests to admin restricted urls', () => {
    const requestToken = jwt.sign({name: 'admin', email: 'name@domain.de', hash: 'password', isAdministrator: true},
      config.jwtSecret);
    return axios.get('http://localhost:7777/adminRequired', {headers: {authorization: 'Bearer ' + requestToken}})
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  it('rejects anonymous requests to admin restricted urls', done => {
    axios.get('http://localhost:7777/adminRequired')
    // $FlowFixMe somehow does not resolve 'fail'
      .then(res => fail('Should have returned an error code, instead responded', res))
      .catch(err => {
        expect(err.response.status).toBe(401);
        done();
      });
  });

  it('rejects unauthorized requests to admin restricted urls', done => {
    const requestToken = jwt.sign({name: 'user', email: 'name@domain.de', hash: 'password'}, config.jwtSecret);
    axios.get('http://localhost:7777/adminRequired', {headers: {authorization: 'Bearer ' + requestToken}})
    // $FlowFixMe somehow does not resolve 'fail'
      .then(res => fail('Should have returned an error code, instead responded', res))
      .catch(err => {
        expect(err.response.status).toBe(401);
        done();
      });
  });
});

function mockPaths(app: express.Application): void {
  app.get('/authRequired', (req: express.Request, res: express.Response) => {
    res.status(200).end();
  });

  app.get('/adminRequired', (req: express.Request, res: express.Response) => {
    res.status(200).end();
  });

}