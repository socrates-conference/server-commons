// @flow

import express from 'express';
import type {Config} from './ConfigType';
import {addMiddleware} from './middleware';
import type {Middleware} from './middleware';
import type {Logger} from './logger';
import consoleLogger from './consoleLogger';

export default class App {
  config: Config;
  logger: Logger;
  app: express.Application;
  server: express.Server;
  isRunning: boolean;

  constructor(config: Config, logger: Logger = consoleLogger) {
    this.isRunning = false;
    this.config = config;
    this.app = express();
    this.app.set('env', config.environment);
    this.logger = logger;
  }

  applyMiddleware = (middleware: Middleware) => {
    addMiddleware(this.app, this.config)(middleware);
  };

  run = (): Promise<void> => {
    return new Promise(resolve => {
      this.isRunning = true;
      this.server = this.app.listen(this.config.server.port, () => {
        this.logger.info(`Server listening on port ${this.config.server.port}`);
        resolve();
      });
    });
  };

  stop = (): Promise<void> => {
    return new Promise(resolve => {
      this.isRunning = false;
      if (this.server) {
        this.server.close(() => {
          this.logger.info('Server shutdown complete.');
          resolve();
        });
      }
    });
  };
}
