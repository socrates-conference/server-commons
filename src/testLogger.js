// @flow
import type {Logger} from './logger';

const testLogger: Logger = {
  error: () => {},
  info: () => {},
  debug: () => {},
  trace: () => {}
};

export default testLogger;