// @flow


const stream = require('socrates-event-stream');

const EventStream = stream.EventStream;
const EventTracingStream = stream.EventTracingStream;

const logger = {
  info: (message, obj) => console.log(message, obj),
  error: (message, obj) => console.error(message, obj),
  trace: (message, ...obj) => {
    if(message.indexOf('Event') === 0) {
      console.log.apply(null, [message].concat(obj));
    }
  }
};
const read = new EventStream([{topic: 'socrates-events'}], 'zookeeper', '2181', 'test-read', logger);
read.on('error', err => logger.error(err));
read.startConsuming();

read.pipe(new EventTracingStream(logger));