// @flow

export type Logger = {
  error: (msg: string, ...args: any[]) => void,
  info: (msg: string, ...args: any[]) => void,
  debug: (msg: string, ...args: any[]) => void,
  trace: (msg: string, ...args: any[]) => void,
}