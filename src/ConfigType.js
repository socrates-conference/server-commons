// @flow
export type Config = {
  environment: string,
  server: {
    port: number;
  },
  auth?: {
    needsAuthentication?: string[],
    needsAdministrator?: string[]
  },
  jwtSecret?: string,
  database?: ExternalService & Credential & {
    name: string,
    debug: false
  },
  kafka?: ExternalService & {
    topics: string[]
  }
}

export type Credential = {
  user: string,
  password: string
}

export type ExternalService = {
  host: string,
  port: number
}
