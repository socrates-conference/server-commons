// @flow

import {EventDispatcher, Event} from './eventDispatcher';

export default class Aggregate {
  _dispatcher: EventDispatcher;

  constructor(dispatcher: EventDispatcher) {
    this._dispatcher = dispatcher;
  }

  // noinspection JSUnusedGlobalSymbols
  dispatch(event: Event) {
    this._dispatcher.dispatchEvent(event);
  }
}