import DomainEventDispatcher from './domainEventDispatcher';

describe('DomainEventDispatcher should', () => {
  let domainEventDispatcher;

  beforeEach(() => {
    domainEventDispatcher = new DomainEventDispatcher();
  });

  it('call a function when a specific event is dispatched', done => {
    domainEventDispatcher.subscribe('test', () => done());
    domainEventDispatcher.dispatchEvent({type: 'test'});
  });

  it('call a function when any event is dispatched', done => {
    domainEventDispatcher.subscribe('*', () => done());
    domainEventDispatcher.dispatchEvent({type: 'test'});
  });

  it('not call a function when event is of different type', () => {
    domainEventDispatcher.subscribe('test', () => fail('Should not be invoked.'));
    domainEventDispatcher.dispatchEvent({type: 'otherEvent'});
  });

  it('call more than one function when an event is dispatched', done => {
    let count = 0;
    const createListenerCallback = () => () => {
      count++;
      if (count === 2) {
        done();
      }
    };
    domainEventDispatcher.subscribe('test', createListenerCallback());
    domainEventDispatcher.subscribe('test', createListenerCallback());

    domainEventDispatcher.dispatchEvent({type: 'test'});
  });

  it('not call a function when it was unsubscribed', () => {
    const listenerCallback = () => fail();
    domainEventDispatcher.subscribe('test', listenerCallback);
    domainEventDispatcher.unsubscribe('test', listenerCallback);

    domainEventDispatcher.dispatchEvent({type: 'test'});
  });

  it('call a function only once when multiple events are dispatched', () => {
    let count = 0;
    const createListenerCallback = () => () => {
      count++;
      if (count === 2) {
        fail();
      }
    };
    domainEventDispatcher.subscribeOnce('test', createListenerCallback());

    domainEventDispatcher.dispatchEvent({type: 'test'});
    domainEventDispatcher.dispatchEvent({type: 'test'});

    expect(count).toEqual(1);
  });

  it('not call a function once when it was unsubscribed', () => {
    const listenerCallback = () => fail();
    domainEventDispatcher.subscribeOnce('test', listenerCallback);
    domainEventDispatcher.unsubscribe('test', listenerCallback);

    domainEventDispatcher.dispatchEvent({type: 'test'});
  });
});
