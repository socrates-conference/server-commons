// @flow
import express from 'express';
import type {Middleware} from '../middleware';
import {verify} from 'jsonwebtoken';
import type {Config} from '../ConfigType';

export type User = {
  name: string,
  email: string,
  password: string,
  isAdministrator: boolean,
  isOneTimePassword: boolean
}
export type AccessCheckResult = {
  allowed: boolean,
  error: ?Error
}

const isJWTAuthorizationAvailable = (req: express.Request) => {
  return req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer';
};

const extractJWTToken = (req: express.Request) => {
  return req.headers.authorization.split(' ')[1];
};

export default function createAuthenticationMiddleware(): Middleware {
  return (app: express.Application, config?: Config) => {
    app.use(async (req: express.Request, res: express.Response, next: express.NextFunction) => {

      if (req.method === 'OPTIONS') {
        return next();
      }
      const requestNeedsAuthentication = config && config.auth && config.auth.needsAuthentication || [];
      const requestNeedsAdministrator = config && config.auth && config.auth.needsAdministrator || [];

      const needsAuthentication = (url: string): boolean => {
        const pattern = new RegExp(requestNeedsAuthentication.join('|'));
        return pattern.test(url.toLowerCase());
      };

      const needsAdministrator = (url: string): boolean => {
        const pattern = new RegExp(requestNeedsAdministrator.join('|'));
        return pattern.test(url.toLowerCase());
      };

      const populateRequestUser = () => {
        if (isJWTAuthorizationAvailable(req)) {
          if (!config || !config.jwtSecret) {
            throw new Error('Configuration error: No JWT secret found.');
          }
          verify(extractJWTToken(req), config.jwtSecret, (err, decode) => {
            req.user = err ? undefined : decode;
          });
        } else {
          req.user = undefined;
        }
      };

      const checkAccess = async (url: string, theUser: ?User): Promise<AccessCheckResult> => {
        const authenticationRequired = needsAuthentication(url);
        const administratorRequired = needsAdministrator(url);

        if (!theUser && (authenticationRequired || administratorRequired)) {
          return {allowed: false, error: new Error('Authentication required.')};
        }
        if (theUser && administratorRequired && !theUser.isAdministrator) {
          return {allowed: false, error: new Error('Administrator required.')};
        }
        return {allowed: true, error: null};
      };
      populateRequestUser();
      const access: AccessCheckResult = await checkAccess(req.url, req.user);
      if (access.allowed) {
        return next();
      } else {
        res.status(401).send(access.error);
      }
    });
  };
}