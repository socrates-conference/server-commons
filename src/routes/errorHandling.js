// @flow
import express from 'express';
import testLogger from '../testLogger';

export const throwServerError = (status: number, message: string) => {
  // This ugly workaround is brought to you by Babel's inability to extend the built-in Error class.
  throw new Error(JSON.stringify({status, message}));
};

export const tryToProcess = (req: express.Request, res: express.Response, action: any => Promise<?number>) => {
  return action(req.body)
    .then(() => res.status(200).end())
    .catch((err: Error) => {
      // This ugly exception handler is brought to you by Babel's inability to extend the built-in Error class.
      try {
        const {status, message} = JSON.parse(err.message);
        return res.status(status).send(message).end();
      } catch (_) {
        testLogger.error('Error while processing request: %o', err);
        return res.status(500).end();
      }
    });
};