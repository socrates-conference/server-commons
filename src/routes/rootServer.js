// @flow
import express from 'express';
import type {Middleware} from '../middleware';

export default function createRootServer(): Middleware {
  return (app: express.Application) => {
    app.get('/', (req: express.Request, res: express.Response) => {
      res.status(200).json({'status': 'Server UP'}).end();
    });
    app.use(pageNotFound);
  };
}

// catch 404 and forward to error handler
function pageNotFound(req: express.Request, res: express.Response) {
  const err = new Error('Not Found');
  // noinspection JSUndefinedPropertyAssignment
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') !== 'prod' ? err : {};
  // render the error page
  res.status(404);
  res.send(req.app.get('env') !== 'prod' ? err : '');
}