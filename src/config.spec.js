// @flow

import config from './config';

describe('config', () => {
  it('contains server object', () => {
    expect(config.server).toBeDefined();
  });
  it('defines server port', () => {
    expect(config.server.port).toBeDefined();
  });
  it('contains environment definition', () => {
    expect(config.environment).toBeDefined();
  });
  it('contains jwt secret definition', () => {
    expect(config.environment).toBeDefined();
  });
  it('contains database object', () => {
    expect(config.jwtSecret).toBeDefined();
  });
  it('defines database host', () => {
    expect(config.database.host).toBeDefined();
  });
  it('defines database port', () => {
    expect(config.database.port).toBeDefined();
  });
  it('defines database name', () => {
    expect(config.database.name).toBeDefined();
  });
  it('defines database user', () => {
    expect(config.database.user).toBeDefined();
  });
  it('defines database password', () => {
    expect(config.database.password).toBeDefined();
  });
});
